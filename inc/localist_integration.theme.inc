<?php
/**
 * @file
 * Defines theme functions for the localist elements.
 */

/**
 * Theme function for localist addresses.
 *
 * @param array $address
 *   An array of address information.
 *
 * @return string
 *   A rendered string for the address.
 */
function theme_localist_address(array &$address) {
  // Create the render array.
  $render = array(
    '#prefix' => '<address>',
    '#suffix' => '</address>',
    'street' => array(
      '#prefix' => '<div class="street">',
      '#suffix' => '</div>',
      '#markup' => $address['street'],
    ),
    'locality' => array(
      '#prefix' => '<div class="locality">',
      '#suffix' => '</div>',
      'city' => array(
        '#prefix' => '<span class="city">',
        '#suffix' => '</span>',
        '#markup' => $address['city'] . ', ',
      ),
      'state' => array(
        '#prefix' => '<span class="state">',
        '#suffix' => '</span>',
        '#markup' => $address['state'] . ' ',
      ),
      'zip' => array(
        '#prefix' => '<span class="zip">',
        '#suffix' => '</span>',
        '#markup' => $address['zip'] . ' ',
      ),
    ),
  );

  // Send back the render array.
  return render($render);
}

/**
 * Theme function for localist event instances.
 *
 * @param mixed $instance
 *   A semi-structured array of event instance information.
 *
 * @return string
 *   A rendered string of event instance information.
 */
function theme_localist_event_instance($instance) {
  if (isset($instance['instance']) and !empty($instance['instance'])) {
    $options = (isset($instance['options'])) ? $instance['options'] : FALSE;
    $instance = $instance['instance'];
  }
  else {
    $options = FALSE;
  }

  // Date format options.
  if (!$options) {
    $timezone = drupal_get_user_timezone();
    $date_format = 'custom';
    $custom = ($instance['end']) ? 'F j, Y g:i a' : 'F j, Y';
  }
  else {
    // Do some date parsing.
    $timezone = (empty($options['timezone'])) ? drupal_get_user_timezone() : $options['timezone'];
    $date_format = $options['date_format'];
    $custom = $options['custom_date_format'];
  }

  $start_format = format_date($instance['start'], $date_format, $custom, $timezone);
  $end_format = ($instance['end']) ? format_date($instance['end'], $date_format, $custom, $timezone) : t('(all day event)');

  // Create the render array.
  $render = array(
    '#prefix' => '<div class="localist-event-instance">',
    '#suffix' => '</div>',
    'start' => array(
      '#prefix' => '<span class="start-time">',
      '#suffix' => '</span>',
      '#markup' => $start_format,
    ),
    'end' => array(
      '#prefix' => '<span class="end-time' . ((!$instance['end']) ? ' all-day' : '') . '">',
      '#suffix' => '</span>',
      '#markup' => $end_format,
    ),
  );

  // If the start & end format match, remove the end.
  if ($start_format == $end_format) {
    unset($render['end']);
  }

  // Send back the render array.
  return render($render);
}
