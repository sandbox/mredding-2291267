<?php
/**
 * @file
 * Defines administration forms for the Localist Integration module.
 */

/**
 * Constants.
 */
if (!defined('LOCALIST_INTEGRATION_FEED_VARIABLE')) {
  define('LOCALIST_INTEGRATION_FEED_VARIABLE', 'localist_integration_feed');
}
if (!defined('LOCALIST_INTEGRATION_IMPORT_DAYS_VARIABLE')) {
  define('LOCALIST_INTEGRATION_IMPORT_DAYS_VARIABLE', 'localist_integration_import_days');
}

/**
 * Implements drupal_get_form(). Defines administration menu and related tasks.
 *
 * @param array $form
 *   A form array passed from drupal_get_form().
 * @param array $form_state
 *   A form_state array passed from drupal_get_form().
 *
 * @return array
 *   A form array.
 */
function localist_integration_admin(array $form, array &$form_state) {
  // Get organization info.
  if ($name = variable_get('localist_organization_name', FALSE)) {
    $form['organization'] = array(
      '#type' => 'markup',
      '#markup' => '<strong>' . t('Organization:') . '</strong> ' . $name,
    );
  }

  // Define the feed URL.
  $form[LOCALIST_INTEGRATION_FEED_VARIABLE] = array(
    '#type' => 'textfield',
    '#title' => t('Localist Feed URL'),
    '#required' => TRUE,
    '#description' => t('The <a href="@feed_api_url" target="_blank">Localist Feed API URL</a> provides an interface between Drupal and the Localist API.', array(
      '@feed_api_url' => 'http://www.localist.com/doc/api#usage',
    )),
    '#default_value' => variable_get(LOCALIST_INTEGRATION_FEED_VARIABLE, ''),
  );

  // Define the number of days to seed.
  $form[LOCALIST_INTEGRATION_IMPORT_DAYS_VARIABLE] = array(
    '#type' => 'select',
    '#title' => t('Number of days to import'),
    '#options' => array(
      '15' => 15,
      '30' => 30,
      '45' => 45,
      '60' => 60,
    ),
    '#default_value' => variable_get(LOCALIST_INTEGRATION_IMPORT_DAYS_VARIABLE, '60'),
  );

  return system_settings_form($form);
}

/**
 * Implements drupal_form_validate(). Makes sure Localist API URL is correct.
 *
 * @param array $form
 *   The form array passed from localist_integration_admin_form().
 * @param array $form_state
 *   The form_state array passed from localist_integration_admin_form().
 *
 * @return void
 */
function localist_integration_admin_validate(array &$form, array &$form_state) {
  $feed_value = $form_state['values'][LOCALIST_INTEGRATION_FEED_VARIABLE];

  // Check to make sure the feed is a valid url.
  if (!valid_url($feed_value, TRUE)) {
    form_set_error(LOCALIST_INTEGRATION_FEED_VARIABLE, t('A valid URL is required.'));
    return;
  }

  // Check to make sure that the feed contains the API key.
  if (!strpos($feed_value, 'api/2')) {
    form_set_error(LOCALIST_INTEGRATION_FEED_VARIABLE, t('A valid Localist Feed URL is required.'));
    return;
  }

  // If there's not a backslash at the end of the URL, add one.
  if (substr($feed_value, -1) != '/') {
    $form_state['values'][LOCALIST_INTEGRATION_FEED_VARIABLE] .= '/';
    $feed_value = $form_state['values'][LOCALIST_INTEGRATION_FEED_VARIABLE];
    return;
  }

  // Make sure there's a Localist feed at the other end of the new value.
  $localist = new LocalistAPI($feed_value);
  if (!$localist->isValidFeed($feed_value)) {
    form_set_error(LOCALIST_INTEGRATION_FEED_VARIABLE, t('A valid Localist Feed URL is required.'));
    return;
  }

  // All validation has succeeded, so continue to the submit handler.
  $form['#submit'][] = 'localist_integration_admin_submit';
}

/**
 * Submit handler to process admin option settings.
 *
 * @param array $form
 *   A form array passed from localist_integration_admin_validate().
 * @param array $form_state
 *   A form_state array passed from localist_integration_admin_validate().
 */
function localist_integration_admin_submit(array $form, array &$form_state) {
  // Variable has already been set, so use that.
  $feed_url = variable_get(LOCALIST_INTEGRATION_FEED_VARIABLE, '');

  // Set the number of days to import.
  variable_set(LOCALIST_INTEGRATION_IMPORT_DAYS_VARIABLE, $form_state['values']['localist_integration_import_days']);

  // Call the organization info organization.
  localist_integration_set_organization($feed_url);
  $localist_api = new LocalistAPI($feed_url);

  // Import localist data.
  localist_integration_import_filters($localist_api);
  localist_integration_import_places($localist_api);
  localist_integration_import_events($localist_api);
}

/**
 * Helper function to process the filter import.
 *
 * @param object $localist_api
 *   A Localist API object.
 */
function localist_integration_import_filters($localist_api = NULL) {
  // If there's no API object being passed, declare a new one.
  if (empty($localist_api)) {
    $localist_api = new LocalistAPI();
  }

  // Get data from the remote server.
  $filter_types = $localist_api->getFilterTypes();
  $filter_values = $localist_api->getFilterValues();

  // Create bundles.
  foreach ($filter_types as $filter_type => $filter_label) {
    // Check to see if the bundle exists.
    $result = db_select('localist_filter_type', 'l')
      ->fields('l')
      ->condition('type', $filter_type, '=')
      ->execute()
      ->fetchAssoc();

    // If the type doesn't exist, add it.
    if ($result['type'] != $filter_type) {
      // Declare the local bundle.
      $bundle = new LocalistFilterType();
      $bundle->type = $filter_type;
      $bundle->label = $filter_label;
      $bundle->weight = 0;

      // Save the new bundle.
      $bundle->save();
    }
  }

  // Loop through the data and insert values.
  foreach ($filter_values as $bundle => $values) {
    foreach ($values as $value) {
      // See if we have a new filter type.
      $result = db_select('localist_filter', 'l')
        ->fields('l')
        ->condition('id', $value['id'])
        ->execute()
        ->fetchAssoc();

      if ($result['id'] != $value['id']) {
        // Get the new filter.
        $filter = new LocalistFilter($value, 'localist_filter');
        $filter->type = $bundle;
        $filter->original = $value;
        $filter->save();
      }
      else {
        // Update the filter.
        $entity = localist_filter_load_by_id($value['id']);
        $entity->name = $value['name'];
        $entity->parent_id = $value['parent_id'];
        $entity->save();
      }
    }
  }
}

/**
 * Helper function to process the places import.
 *
 * @param object $localist_api
 *   A Localist API object.
 */
function localist_integration_import_places($localist_api) {
  // If there's no API object being passed, declare a new one.
  if (empty($localist_api)) {
    $localist_api = new LocalistAPI();
  }

  // Grab the place builds.
  if ($response = $localist_api->getPlacesPages()) {
    // Grab the page info.
    $current_page = $response['current'];
    $total_pages = $response['total'];
    $per_page = $response['size'];

    // Save the new place bundle, if necessary.
    $result = db_select('localist_place_type', 'l')
      ->fields('l')
      ->condition('type', 'generic_place', '=')
      ->execute()
      ->fetchAssoc();

    // If the type doesn't exist, add it.
    if ($result['type'] != 'generic_place') {
      $bundle = new LocalistPlaceType();
      $bundle->type = 'generic_place';
      $bundle->label = t('Generic Place');
      $bundle->weight = 0;
      $bundle->save();
    }

    // Set the batch operations.
    $batch = array(
      'operations' => array(
        array('localist_integration_process_places', array(
          $current_page,
          $total_pages,
          $per_page,
          $localist_api,
        )),
      ),
      'finished' => 'localist_integration_process_places_finished',
      'title' => t('Importing Localist Places'),
      'init_message' => t('Localist Places import is starting.'),
      'progress_message' => t('Processing Localist Places import.'),
      'error_message' => t('Localist Places import has encountered an error.'),
      'file' => drupal_get_path('module', LOCALIST_INTEGRATION_MODULE_NAME) . '/localist_places/localist_place.batch.inc',
    );

    batch_set($batch);
  }
}

/**
 * Helper function to process the events import.
 *
 * @param object $localist_api
 *   A Localist API object.
 */
function localist_integration_import_events($localist_api) {
  // If there's no API object being passed, declare a new one.
  if (empty($localist_api)) {
    $localist_api = new LocalistAPI();
  }

  // Grab the place builds.
  if ($response = $localist_api->getEventsPages()) {
    // Grab the page info.
    $current_page = $response['current'];
    $total_pages = $response['total'];
    $per_page = $response['size'];

    // Save the new place bundle, if necessary.
    $result = db_select('localist_event_type', 'l')
      ->fields('l')
      ->condition('type', 'generic_event', '=')
      ->execute()
      ->fetchAssoc();

    // If the type doesn't exist, add it.
    if ($result['type'] != 'generic_event') {
      $bundle = new LocalistEventType();
      $bundle->type = 'generic_event';
      $bundle->label = t('Generic Event');
      $bundle->weight = 0;
      $bundle->save();
    }

    // Set the batch operations.
    $batch = array(
      'operations' => array(
        array('localist_integration_process_events', array(
          $current_page,
          $total_pages,
          $per_page,
          $localist_api,
        )),
      ),
      'finished' => 'localist_integration_process_events_finished',
      'title' => t('Importing Localist Events'),
      'init_message' => t('Localist Events import is starting.'),
      'progress_message' => t('Processing Localist Events import.'),
      'error_message' => t('Localist Events import has encountered an error.'),
      'file' => drupal_get_path('module', LOCALIST_INTEGRATION_MODULE_NAME) . '/localist_events/localist_event.batch.inc',
    );

    batch_set($batch);
  }
}
