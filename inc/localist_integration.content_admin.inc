<?php
/**
 * @file
 * Localist content administration and module settings UI.
 */

/**
 * Page callback: Form constructor for the content administration form.
 *
 * @ingroup forms
 */
function localist_integration_admin_event_content($form, &$form_state) {
  $form['admin'] = localist_integration_admin_events();

  return $form;
}

/**
 * Page callback: Form constructor for the content administration form.
 *
 * @ingroup forms
 */
function localist_integration_admin_filter_content($form, &$form_state) {
  $form['admin'] = localist_integration_admin_filters();

  return $form;
}

/**
 * Page callback: Form constructor for the content administration form.
 *
 * @ingroup forms
 */
function localist_integration_admin_place_content($form, &$form_state) {
  $form['admin'] = localist_integration_admin_places();

  return $form;
}

/**
 * Form builder: Builds the edit administration overview.
 */
function localist_integration_admin_events() {
  if ($admin_access = user_access(LOCALIST_INTEGRATION_PERMISSION_EDIT_EVENTS)) {
    // Build the sortable table header.
    $header = array(
      'name' => array('data' => t('Title'), 'field' => 'l.name'),
      'start' => array('data' => t('First Start'), 'field' => 'i.start'),
      'operations' => array('data' => t('Operations')),
    );
    $query = db_select('localist_event', 'l')->extend('PagerDefault')->extend('TableSort');
    $query->join('localist_event_instance_data', 'i', 'i.event_leid = l.leid');
    $query->groupBy('l.leid');

    $leids = $query
      ->fields('l', array('leid'))
      ->fields('i', array('start'))
      ->limit(20)
      ->orderByHeader($header)
      ->execute()
      ->fetchCol();
    $events = localist_event_load_multiple($leids);

    // Prepare the list of nodes.
    foreach ($events as $event) {
      // Figure out the date.
      $date = ($event->instances[0]['all_day']) ? format_date($event->instances[0]['start'], 'custom', 'Y-m-d') . ' (all day)' : format_date($event->instances[0]['start'], 'custom', 'Y-m-d g:i a');

      $options[$event->leid] = array(
        'name' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $event->name,
            '#href' => 'localist/event/' . $event->leid,
            '#options' => array(
              'html' => TRUE,
            ),
          ),
        ),
        'start' => $date,
      );

      // Build a list of all the accessible operations for the current node.
      $operations = array();
      $destination = drupal_get_destination();
      if (localist_filter_access('edit', $event)) {
        $operations['edit'] = array(
          'title' => t('edit'),
          'href' => 'localist/event/' . $event->leid . '/edit',
          'query' => $destination,
        );
      }
      if (localist_filter_access('view', $event)) {
        $operations['view'] = array(
          'title' => t('view'),
          'href' => 'localist/event/' . $event->leid,
        );
      }

      // Render the operations links.
      $options[$event->leid]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }

    // Only use a tableselect when the current user is able to perform any
    // operations.
    $form['content'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );

    $form['pager'] = array('#markup' => theme('pager'));
  }

  return $form;
}

/**
 * Form builder: Builds the node administration overview.
 */
function localist_integration_admin_filters() {
  if ($admin_access = user_access(LOCALIST_INTEGRATION_PERMISSION_EDIT_FILTERS)) {
    // Build the sortable table header.
    $header = array(
      'name' => array('data' => t('Title'), 'field' => 'l.name'),
      'type' => array('data' => t('Type'), 'field' => 'f.label'),
      'operations' => array('data' => t('Operations')),
    );
    $query = db_select('localist_filter', 'l')->extend('PagerDefault')->extend('TableSort');
    $query->join('localist_filter_type', 'f', 'l.type = f.type');

    $lfids = $query
      ->fields('l', array('lfid'))
      ->fields('f', array('label'))
      ->limit(20)
      ->orderByHeader($header)
      ->execute()
      ->fetchCol();
    $filters = localist_filter_load_multiple($lfids);

    // Prepare the list of nodes.
    foreach ($filters as $filter) {
      $options[$filter->lfid] = array(
        'name' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $filter->name,
            '#href' => 'localist/filter/' . $filter->lfid,
          ),
        ),
        'type' => check_plain($filter->getTypeLabel()),
      );

      // Build a list of all the accessible operations for the current node.
      $operations = array();
      $destination = drupal_get_destination();
      if (localist_filter_access('edit', $filter)) {
        $operations['edit'] = array(
          'title' => t('edit'),
          'href' => 'localist/filter/' . $filter->lfid . '/edit',
          'query' => $destination,
        );
      }
      if (localist_filter_access('view', $filter)) {
        $operations['view'] = array(
          'title' => t('view'),
          'href' => 'localist/filter/' . $filter->lfid,
        );
      }

      // Render the operations links.
      $options[$filter->lfid]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }

    // Only use a tableselect when the current user is able to perform any
    // operations.
    $form['content'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );

    $form['pager'] = array('#markup' => theme('pager'));
  }

  return $form;
}

/**
 * Form builder: Builds the node administration overview.
 */
function localist_integration_admin_places() {
  if ($admin_access = user_access(LOCALIST_INTEGRATION_PERMISSION_EDIT_PLACES)) {
    // Build the sortable table header.
    $header = array(
      'name' => array('data' => t('Title'), 'field' => 'l.name'),
      'operations' => array('data' => t('Operations')),
    );
    $query = db_select('localist_place', 'l')->extend('PagerDefault')->extend('TableSort');

    $lpids = $query
      ->fields('l', array('lpid'))
      ->limit(20)
      ->orderByHeader($header)
      ->execute()
      ->fetchCol();
    $places = localist_place_load_multiple($lpids);

    // Prepare the list of nodes.
    foreach ($places as $place) {
      $options[$place->lpid] = array(
        'name' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $place->name,
            '#href' => 'localist/place/' . $place->lpid,
          ),
        ),
      );

      // Build a list of all the accessible operations for the current node.
      $operations = array();
      $destination = drupal_get_destination();
      if (localist_place_access('edit', $place)) {
        $operations['edit'] = array(
          'title' => t('edit'),
          'href' => 'localist/place/' . $place->lpid . '/edit',
          'query' => $destination,
        );
      }
      if (localist_filter_access('view', $place)) {
        $operations['view'] = array(
          'title' => t('view'),
          'href' => 'localist/place/' . $place->lpid,
        );
      }

      // Render the operations links.
      $options[$place->lpid]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }

    // Only use a tableselect when the current user is able to perform any
    // operations.
    $form['content'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );

    $form['pager'] = array('#markup' => theme('pager'));
  }

  return $form;
}
