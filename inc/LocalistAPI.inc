<?php
/**
 * @file
 * An API wrapper for the Localist API.
 */

/**
 * Class definition for the localist API.
 */
class LocalistAPI {
  const RESOURCE_ORGANIZATION = 'organizations';
  const RESOURCE_COMMUNITIES = 'communities';
  const RESOURCE_EVENTS = 'events';
  const RESOURCE_PLACES = 'places';
  const RESOURCE_GROUPS = 'groups';
  const RESOURCE_DEPARTMENTS = 'departments';
  const RESOURCE_PHOTOS = 'photos';

  /**
   * Feed URL.
   *
   * @var string
   */
  public $feedUrl;

  /**
   * Organization ID.
   *
   * @var int
   */
  public $organizationId;

  /**
   * Number of days to import.
   *
   * @var int
   */
  protected $daysToImport;

  /**
   * Default constructor.
   *
   * @param string $feed_url
   *   The feed URL for the Localist API.
   * @param int $organization_id
   *   An integer representing the remote organization ID.
   */
  public function __construct($feed_url = NULL, $organization_id = 0) {
    // Set the variables.
    $this->feedUrl = (is_null($feed_url)) ? variable_get('localist_integration_feed', '') : $feed_url;
    $this->organizationId = ($organization_id == 0) ? variable_get('localist_organization_id', 0) : $organization_id;

    // Set the days to import.
    $this->daysToImport = variable_get('localist_integration_import_days', 60);
  }

  /**
   * Internal function to grab a specified resource.
   *
   * @param string $resource
   *   The remote resource to consume.
   * @param array $parameters
   *   The parameters to consume.
   *
   * @return mixed
   *   Data from the resource.
   */
  protected function getResource($resource, $parameters = array()) {
    // Build the request.
    $response = drupal_http_request(url($this->feedUrl . $resource, array('query' => $parameters)));

    // If the response is OK, return an array with that object.
    if ($response->code == 200) {
      return drupal_json_decode($response->data);
    }

    // There was some general error.
    return FALSE;
  }

  /**
   * Function to determine if the feed URL is valid.
   *
   * @return bool
   *   Returns true if the feed is a valid feed, false if it's not.
   */
  public function isValidFeed() {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_ORGANIZATION)) {
      if (isset($response['organizations'])) {
        return TRUE;
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to get organization resource.
   *
   * @return mixed
   *   Returns organization resource data.
   */
  public function getOrganization() {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_ORGANIZATION)) {
      if (isset($response['organizations'])) {
        return $response['organizations'][0]['organization'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to get places resource.
   *
   * @param int $page
   *   The page to return data for.
   *
   * @return mixed
   *   Returns place resource data.
   */
  public function getPlaces($page = 1) {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_PLACES, array('page' => $page))) {
      if (isset($response['places'])) {
        return $response['places'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to get the number of pages of places.
   *
   * @return mixed
   *   Returns an integer with the number of pages, or false on failure.
   */
  public function getPlacesPages() {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_PLACES)) {
      if (isset($response['page'])) {
        return $response['page'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to get events resource.
   *
   * @param int $page
   *   The page to return event resources for.
   *
   * @return mixed
   *   Returns false if failure, or resource data.
   */
  public function getEvents($page = 1) {
    $response = $this->getResource(LocalistAPI::RESOURCE_EVENTS, array(
      'page' => $page,
      'pp' => 25,
      'days' => $this->daysToImport,
    ));

    if ($response) {
      if (isset($response['events'])) {
        return $response['events'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to get the number of pages of events.
   *
   * @return mixed
   *   Returns an integer with the number of pages, or false on failure.
   */
  public function getEventsPages() {
    $response = $this->getResource(LocalistAPI::RESOURCE_EVENTS, array(
      'pp' => 25,
      'days' => $this->daysToImport,
    ));

    if ($response) {
      if (isset($response['page'])) {
        return $response['page'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to grab the filter types.
   *
   * @return mixed
   *   False on general failure, or an array of filter type data.
   */
  public function getFilterTypes() {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_EVENTS . '/labels')) {
      if (isset($response['filters'])) {
        return $response['filters'];
      }
    }

    // General failure.
    return FALSE;
  }

  /**
   * Function to grab the filter values.
   *
   * @return mixed
   *   Returns false on general failure, or an array of filter objects.
   */
  public function getFilterValues() {
    if ($response = $this->getResource(LocalistAPI::RESOURCE_EVENTS . '/filters')) {
      return $response;
    }

    // General failure.
    return FALSE;
  }

}
