<?php
/**
 * @file
 * Define Entity API calls.
 */

/**
 * Load a Localist Event.
 */
function localist_event_load($leid, $reset = FALSE) {
  $events = localist_event_load_multiple(array($leid), array(), $reset);
  return reset($events);
}

/**
 * Load a Localist Event by ID.
 *
 * @param int $id
 *   The Localist API id for an event.
 *
 * @return mixed
 *   Returns false if no entity can be found with the API id. Returns a
 *   Localist Event object if an event can be found.
 */
function localist_event_load_by_id($id) {
  // Get an entity field query.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'localist_event')
    ->propertyCondition('id', $id);
  $result = $efq->execute();

  // If we have a result, go ahead and try to return a loaded Localist Event
  // object.
  if (isset($result['localist_event'])) {
    $result = array_values($result['localist_event']);
    return localist_event_load($result[0]->leid);
  }

  return FALSE;
}

/**
 * Load multiple Localist Events based on certain conditions.
 */
function localist_event_load_multiple($leids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('localist_event', $leids, $conditions, $reset);
}

/**
 * Save Localist Event.
 */
function localist_event_save($localist_event) {
  entity_save('localist_event', $localist_event);
}

/**
 * Delete single Localist Event.
 */
function localist_event_delete($localist_event) {
  entity_delete('localist_event', entity_id('localist_event', $localist_event));
}

/**
 * Delete multiple Localist Events.
 */
function localist_event_delete_multiple($localist_event_ids) {
  entity_delete_multiple('localist_event', $localist_event_ids);
}

/**
 * Load Localist Event Type.
 */
function localist_event_type_load($localist_event_type) {
  return localist_event_types($localist_event_type);
}

/**
 * List of Localist Event Types.
 */
function localist_event_types($localist_event_type = NULL) {
  $types = entity_load_multiple_by_name('localist_event_type', isset($localist_event_type) ? array($localist_event_type) : FALSE);
  return isset($localist_event_type) ? reset($types) : $types;
}

/**
 * Save Localist Event Type entity.
 */
function localist_event_type_save($localist_event_type) {
  entity_save('localist_event_type', $localist_event_type);
}

/**
 * Delete single case type.
 */
function localist_event_type_delete($localist_event_type) {
  entity_delete('localist_event_type', entity_id('localist_event_type', $localist_event_type));
}

/**
 * Delete multiple case types.
 */
function localist_event_type_delete_multiple($localist_event_type_ids) {
  entity_delete_multiple('localist_event_type', $localist_event_type_ids);
}


/**
 * Load a Localist Filter.
 */
function localist_filter_load($lfid, $reset = FALSE) {
  $filters = localist_filter_load_multiple(array($lfid), array(), $reset);
  return reset($filters);
}

/**
 * Load a Localist Filter by ID.
 *
 * @param int $id
 *   The Localist API id for a filter.
 *
 * @return mixed
 *   Returns false if no entity can be found with the API id. Returns a
 *   Localist Filter object if an event can be found.
 */
function localist_filter_load_by_id($id) {
  // Get an entity field query.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'localist_filter')
    ->propertyCondition('id', $id);
  $result = $efq->execute();

  // If we have a result, go ahead and try to return a loaded Localist Event
  // object.
  if (isset($result['localist_filter'])) {
    $result = array_values($result['localist_filter']);
    return localist_filter_load($result[0]->lfid);
  }

  return FALSE;
}

/**
 * Load multiple Localist Filters based on certain conditions.
 */
function localist_filter_load_multiple($lfids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('localist_filter', $lfids, $conditions, $reset);
}

/**
 * Save Localist Filter.
 */
function localist_filter_save($localist_filter) {
  entity_save('localist_filter', $localist_filter);
}

/**
 * Delete single Localist Filter.
 */
function localist_filter_delete($localist_filter) {
  entity_delete('localist_filter', entity_id('localist_filter', $localist_filter));
}

/**
 * Delete multiple Localist Filters.
 */
function localist_filter_delete_multiple($localist_filter_ids) {
  entity_delete_multiple('localist_filter', $localist_filter_ids);
}

/**
 * Load Localist Filter Type.
 */
function localist_filter_type_load($localist_filter_type) {
  return localist_filter_types($localist_filter_type);
}

/**
 * List of Localist Filter Types.
 */
function localist_filter_types($localist_filter_type = NULL) {
  $types = entity_load_multiple_by_name('localist_filter_type', isset($localist_filter_type) ? array($localist_filter_type) : FALSE);
  return isset($localist_filter_type) ? reset($types) : $types;
}

/**
 * Save Localist Filter Type entity.
 */
function localist_filter_type_save($localist_filter_type) {
  entity_save('localist_filter_type', $localist_filter_type);
}

/**
 * Delete single case type.
 */
function localist_filter_type_delete($localist_filter_type) {
  entity_delete('localist_filter_type', entity_id('localist_filter_type', $localist_filter_type));
}

/**
 * Delete multiple case types.
 */
function localist_filter_type_delete_multiple($localist_filter_type_ids) {
  entity_delete_multiple('localist_filter_type', $localist_filter_type_ids);
}

/**
 * Load a Localist Place.
 */
function localist_place_load($lpid, $reset = FALSE) {
  $places = localist_place_load_multiple(array($lpid), array(), $reset);
  return reset($places);
}

/**
 * Load a Localist Place by ID.
 *
 * @param int $id
 *   The Localist API id for a place.
 *
 * @return mixed
 *   Returns false if no entity can be found with the API id. Returns a
 *   Localist Place object if an place can be found.
 */
function localist_place_load_by_id($id) {
  // Get an entity field query.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'localist_place')
    ->propertyCondition('id', $id);
  $result = $efq->execute();

  // If we have a result, go ahead and try to return a loaded Localist Event
  // object.
  if (isset($result['localist_place'])) {
    $result = array_values($result['localist_place']);
    return localist_place_load($result[0]->lpid);
  }

  return FALSE;
}

/**
 * Load multiple Localist Places based on certain conditions.
 */
function localist_place_load_multiple($lpids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('localist_place', $lpids, $conditions, $reset);
}

/**
 * Save Localist Place.
 */
function localist_place_save($localist_place) {
  entity_save('localist_place', $localist_place);
}

/**
 * Delete single Localist Place.
 */
function localist_place_delete($localist_place) {
  entity_delete('localist_place', entity_id('localist_place', $localist_place));
}

/**
 * Delete multiple Localist Places.
 */
function localist_place_delete_multiple($localist_place_ids) {
  entity_delete_multiple('localist_place', $localist_place_ids);
}

/**
 * Load Localist Place Type.
 */
function localist_place_type_load($localist_place_type) {
  return localist_place_types($localist_place_type);
}

/**
 * List of Localist Place Types.
 */
function localist_place_types($localist_place_type = NULL) {
  $types = entity_load_multiple_by_name('localist_place_type', isset($localist_place_type) ? array($localist_place_type) : FALSE);
  return isset($localist_place_type) ? reset($types) : $types;
}

/**
 * Save Localist Place Type entity.
 */
function localist_place_type_save($localist_place_type) {
  entity_save('localist_place_type', $localist_place_type);
}

/**
 * Delete single case type.
 */
function localist_place_type_delete($localist_place_type) {
  entity_delete('localist_place_type', entity_id('localist_place_type', $localist_place_type));
}

/**
 * Delete multiple case types.
 */
function localist_place_type_delete_multiple($localist_place_type_ids) {
  entity_delete_multiple('localist_place_type', $localist_place_type_ids);
}
