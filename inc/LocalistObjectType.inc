<?php
/**
 * @file
 * Defines common classes to be used across all types.
 */

/**
 * LocalistObjectTypeUIController class.
 */
class LocalistObjectTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides default constructor.
   *
   * @param string $entity_type
   *   The type of entity.
   * @param array $entity_info
   *   The entity information.
   */
  public function __construct($entity_type, $entity_info) {
    parent::__construct($entity_type, $entity_info);

    // Set the plural label.
    $this->label = $this->entityInfo['label'];
    $this->pluralLabel = isset($this->entityInfo['plural label']) ? $this->entityInfo['plural label'] : $this->entityInfo['label'] . 's';
  }

  /**
   * Override the basic menu structure to remove default add content type forms.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';
    $plural_label = $this->pluralLabel;

    $items[$this->path] = array(
      'title' => $plural_label,
      'page callback' => 'drupal_get_form',
      'page arguments' => array($this->entityType . '_overview_form', $this->entityType),
      'description' => 'Manage ' . $plural_label . '.',
      'access callback' => 'entity_access',
      'access arguments' => array('view', $this->entityType),
      'file' => 'includes/entity.ui.inc',
    );
    $items[$this->path . '/list'] = array(
      'title' => 'List',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    $items[$this->path . '/manage/' . $wildcard] = array(
      'title' => 'Edit',
      'title callback' => 'entity_label',
      'title arguments' => array($this->entityType, $id_count + 1),
      'page callback' => 'entity_ui_get_form',
      'page arguments' => array($this->entityType, $id_count + 1),
      'load arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('update', $this->entityType, $id_count + 1),
    );
    $items[$this->path . '/manage/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'load arguments' => array($this->entityType),
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );

    if (!empty($this->entityInfo['admin ui']['file'])) {
      // Add in the include file for the entity form.
      foreach (array("/manage/$wildcard") as $path_end) {
        $items[$this->path . $path_end]['file'] = $this->entityInfo['admin ui']['file'];
        $items[$this->path . $path_end]['file path'] = isset($this->entityInfo['admin ui']['file path']) ? $this->entityInfo['admin ui']['file path'] : drupal_get_path('module', $this->entityInfo['module']);
      }
    }
    return $items;
  }

  /**
   * Builds the entity overview form.
   *
   * @param array $form
   *   The Form API form.
   * @param array $form_state
   *   The Form State array.
   * @return array
   *   A form API form.
   */
  public function overviewForm($form, &$form_state) {
    // If there's no types, show a general alert.
    $types = entity_load_multiple_by_name($this->entityType);

    if (empty($types)) {
      drupal_set_message(t('You must configure the <a href="@link">Localist Feed</a> before adding fields to @type.', array(
        '@link' => url('admin/config/content/localist'),
        '@type' => $this->pluralLabel,
      )), 'warning');

      return $form;
    }

    // Load the default manage table for the overview.
    $form['table'] = $this->overviewTable();
    $form['pager'] = array('#theme' => 'pager');
    return $form;
  }

  /**
   * Overrides from EntityDefaultUIController().
   *
   * Generates the row for the passed entity and may be overridden in order to
   * customize the rows.
   *
   * @param $additional_cols
   *   Additional columns to be added after the entity label column.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $entity_uri = entity_uri($this->entityType, $entity);

    $row[] = array('data' => array(
      '#theme' => 'entity_ui_overview_item',
      '#label' => entity_label($this->entityType, $entity),
      '#name' => !empty($this->entityInfo['exportable']) ? entity_id($this->entityType, $entity) : FALSE,
      '#url' => $entity_uri ? $entity_uri : FALSE,
      '#entity_type' => $this->entityType),
    );

    // Add in any passed additional cols.
    foreach ($additional_cols as $col) {
      $row[] = $col;
    }

    // Add a row for the exportable status.
    if (!empty($this->entityInfo['exportable'])) {
      $row[] = array('data' => array(
        '#theme' => 'entity_status',
        '#status' => $entity->{$this->statusKey},
      ));
    }

    // In case this is a bundle, we add links to the field ui tabs.
    $field_ui = !empty($this->entityInfo['bundle of']) and entity_type_is_fieldable($this->entityInfo['bundle of']) and module_exists('field_ui');

    // If i18n integration is enabled, add a link to the translate tab.
    $i18n = !empty($this->entityInfo['i18n controller class']);

    // Add operations depending on the status.
    if ($field_ui) {
      $row[] = l(t('manage fields'), $this->path . '/manage/' . $id . '/fields');
      $row[] = l(t('manage display'), $this->path . '/manage/' . $id . '/display');
    }
    if ($i18n) {
      $row[] = l(t('translate'), $this->path . '/manage/' . $id . '/translate');
    }

    return $row;
  }

}
