<?php
/**
 * @file
 * Defines batch function to import places.
 */

/**
 * Batch API callback to handle importing Localist places.
 *
 * @param int $current_page
 *   The current page of the batch upload.
 * @param int $total_pages
 *   The total number of pages in the batch upload.
 * @param int $per_page
 *   The number of events to process per page.
 * @param object $localist_api
 *   The Localist API object.
 * @param array &$context
 *   The Batch API context array.
 */
function localist_integration_process_places($current_page, $total_pages, $per_page, $localist_api, array &$context) {
  // Set up the sandbox.
  if (!isset($context['sandbox']['current_page'])) {
    $context['sandbox']['current_page'] = $current_page;
    $context['sandbox']['total_pages'] = $total_pages;
    $context['sandbox']['per_page'] = $per_page;
    $context['sandbox']['current_item'] = 0;
    $context['sandbox']['localist_api'] = $localist_api;
  }

  // Grab the resultset.
  $places = $context['sandbox']['localist_api']->getPlaces($context['sandbox']['current_page']);

  // Save each part of the resultset.
  foreach ($places as $place) {
    // First of all, the API returns weird results at times. See if the place
    // already exists because this may actually just be a new instance of the
    // place.
    $entity = localist_place_load_by_id($place['place']['id']);
    if (!$entity) {
      // This entity doesn't exist yet, so let's go ahead and create a new one
      // and save.
      $entity = new stdClass();
      $entity->id = $place['place']['id'];
      $entity->type = 'generic_place';
      $entity->language = LANGUAGE_NONE;
    }

    $entity->name = check_plain($place['place']['name']);
    $entity->description = check_plain($place['place']['description_text']);
    $entity->place_type = $place['place']['type'];
    $entity->created = strtotime($place['place']['created_at']);
    $entity->updated = strtotime($place['place']['updated_at']);
    $entity->geo = $place['place']['geo'];
    $entity->urlname = $place['place']['urlname'];
    $entity->photo_url = $place['place']['photo_url'];
    $entity->original = $place['place'];

    entity_get_controller('localist_place')->save($entity);

    // Attach the name in the resultset.
    $context['results'][] = check_plain($entity->name);

    // Update the progress.
    $context['sandbox']['current_item'] = $entity->id;
    $context['message'] = t('Now processing @place.', array('@place' => $entity->name));
  }

  // Increment the current page.
  $context['sandbox']['current_page']++;
  if ($context['sandbox']['current_page'] <= $context['sandbox']['total_pages']) {
    $context['finished'] = $context['sandbox']['current_page'] / ($context['sandbox']['total_pages'] + 1);
  }
}

/**
 * The Batch API finished callback.
 *
 * @param bool $success
 *   Whether or not the batch API completed successfully.
 * @param array $results
 *   An array of results.
 * @param array $operations
 *   An array of operations.
 */
function localist_integration_process_places_finished($success, array $results, array $operations) {
  if ($success) {
    // Get a new message.
    $message = count($results) . ' places processed.';
    drupal_set_message($message);
  }
  else {
    // Unspecified error.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1],
    TRUE)));

    drupal_set_message($message, 'error');
  }
}
