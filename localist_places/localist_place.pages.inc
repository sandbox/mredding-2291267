<?php
/**
 * @file
 * Defines the place view page.
 */

/**
 * Displays the localist place.
 *
 * @param int $lpid
 *   An lpid to load.
 *
 * @return array
 *   A render array of the current place.
 */
function localist_place_view($lpid) {
  // Load the entity.
  $place = entity_metadata_wrapper('localist_place', $lpid);

  // Set the title.
  drupal_set_title($place->name->value(), PASS_THROUGH);

  return $place->view();
}
