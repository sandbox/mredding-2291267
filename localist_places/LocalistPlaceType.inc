<?php
/**
 * @file
 * Defines place bundle information.
 */

/**
 * LocalistPlaceType class.
 */
class LocalistPlaceType extends Entity {
  /**
   * The bundle name.
   *
   * @var string
   */
  public $type;

  /**
   * The bundle label.
   *
   * @var string
   */
  public $label;

  /**
   * The weight of the bundle.
   *
   * @var int
   */
  public $weight = 0;

  /**
   * Constructor.
   *
   * @param array $values
   *   An array of bundle values.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'localist_place_type');
  }

  /**
   * Function to determine if the bundle is locked for editing.
   *
   * @return bool
   *   Returns true if locked, or false if not locked.
   */
  protected function isLocked() {
    // Check to see if this entity is locked or not.
    return isset($this->status) and empty($this->is_new) and (($this->status & ENTITY_IN_CODE) or ($this->status & ENTITY_FIXED));
  }
}

/**
 * LocalistPlaceTypeController class.
 */
class LocalistPlaceTypeController extends EntityAPIControllerExportable {
  /**
   * Override the create method.
   *
   * @param array $values
   *   An array of values.
   *
   * @return object
   *   The Localist Place Type object.
   */
  public function create(array $values = array()) {
    return parent::create($values);
  }

  /**
   * Override the save method.
   *
   * @param object $entity
   *   The entity object.
   * @param DatabaseTransaction $transaction
   *   The database transaction.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);

    // Alert the system that we need to rebuild the system.
    // @see _http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }

}

/**
 * LocalistPlaceTypeUIController class.
 */
class LocalistPlaceTypeUIController extends LocalistObjectTypeUIController {
}
