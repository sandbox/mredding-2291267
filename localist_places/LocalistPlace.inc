<?php
/**
 * @file
 * The Localist Place class.
 */

/**
 * Localist Place class.
 */
class LocalistPlace extends Entity {

  /**
   * Displays the default label for the entity.
   *
   * @return string
   *   The label.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * The default URI for the entity.
   *
   * @return string
   *   The entity URI.
   */
  protected function defaultUri() {
    return array('path' => 'localist/place/' . $this->identifier());
  }

}

/**
 * Localist Place Controller.
 */
class LocalistPlaceController extends EntityAPIController {
  /**
   * Builds the content for the Localist Event object.
   *
   * @param object $entity
   *   The Localist Event object.
   * @param string $view_mode
   *   The view mode to display.
   * @param string $langcode
   *   The language code to use.
   * @param array $content
   *   An array of content to display.
   *
   * @return array
   *   A render array of content.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Get a wrapper around the entity.
    $wrapper = entity_metadata_wrapper($this->entityType, $entity);

    // Attach place description.
    if (!empty($entity->description)) {
      $content['description'] = array(
        '#title' => t('Description'),
        '#field_name' => 'description',
        '#field_type' => 'text_long',
        '#items' => array(array('value' => $wrapper->description->value())),
        0 => array(
          '#markup' => check_markup($wrapper->description->value(), 'localist_description'),
        ),
      );
    }

    // Attach place type.
    if (!empty($entity->type)) {
      $content['place_type'] = array(
        '#title' => t('Type'),
        '#field_name' => 'place_type',
        '#field_type' => 'text',
        '#items' => array(array('value' => $wrapper->place_type->value())),
        0 => array(
          '#markup' => $wrapper->place_type->value(),
        ),
      );
    }

    // Attach photo.
    if (!empty($entity->photo_url)) {
      $content['photo'] = array(
        '#title' => t('Photo'),
        '#field_name' => 'photo',
        '#field_type' => 'image',
        '#items' => array(array('value' => $wrapper->photo_url->value())),
        0 => array(
          '#markup' => theme('image', array(
            'path' => $wrapper->photo_url->value(),
            'width' => NULL,
            'height' => NULL,
            'alt' => $wrapper->name->value(),
            'attributes' => array(
              'class' => array('localist-photo', 'localist-location-photo'),
            ),
          )),
        ),
      );
    }

    // Attach address.
    if (!empty($entity->geo)) {
      $content['address'] = array(
        '#title' => t('Address'),
        '#field_name' => 'address',
        '#field_type' => 'localist_address',
        '#items' => array(array('value' => $wrapper->geo->value())),
        0 => array(
          '#markup' => theme('localist_address', $wrapper->geo->value()),
        ),
      );
    }

    // Attach common parameters.
    $weight = count($content) * -1;
    list(, , $bundle) = entity_extract_ids($this->entityType, $entity);
    foreach ($content as &$item) {
      $item += array(
        '#theme' => 'field',
        '#weight' => $weight,
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => $wrapper->language->value(),
        '#bundle' => $bundle,
        '#entity_type' => $this->entityType,
      );

      // Increment the weight.
      $weight++;
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}
