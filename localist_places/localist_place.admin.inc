<?php
/**
 * @file
 * Performs administration tasks for event objects.
 */

/**
 * Form API callback to generate the place type edit form.
 *
 * @param array $form
 *   The Form API array.
 * @param array &$form_state
 *   The Form API form state array.
 * @param string $form_id
 *   The form id.
 *
 * @return array
 *   The form.
 */
function localist_place_type_form(array $form, array &$form_state, $form_id) {
  return $form;
}

/**
 * Form API callback to generate the place edit form.
 *
 * @param array $form
 *   The Form API array.
 * @param array &$form_state
 *   The Form API form state array.
 * @param object $localist_place
 *   The Localist Filter object.
 *
 * @return array
 *   The form.
 */
function localist_place_form(array $form, array &$form_state, $localist_place) {
  $build = array();
  $wrapper = entity_metadata_wrapper('localist_place', $localist_place);

  // Set the title.
  drupal_set_title(t('Edit <em>!place_name</em>', array('!place_name' => $wrapper->name->value())), PASS_THROUGH);

  // Set the un-editable field properties.
  $build['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $wrapper->name->value(),
  );
  $build['description'] = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#markup' => ($wrapper->description->value()) ? check_markup($wrapper->description->value(), 'localist_description') : '&lt;' . t('Not set') . '&gt;',
  );
  $build['place_type'] = array(
    '#type' => 'item',
    '#title' => t('Place Type'),
    '#markup' => $wrapper->place_type->value(),
  );
  $build['photo'] = array(
    '#type' => 'item',
    '#title' => t('Photo'),
    '#markup' => ($wrapper->photo_url->value()) ? theme('image', array(
      'path' => $wrapper->photo_url->value(),
      'width' => NULL,
      'height' => NULL,
      'alt' => $wrapper->name->value(),
      'attributes' => array(
        'class' => array('localist-photo', 'localist-location-photo'),
      ),
      )) : '&lt;' . t('Not set') . '&gt;',
  );
  $build['address'] = array(
    '#type' => 'item',
    '#title' => t('Address'),
    '#markup' => ($wrapper->geo->value()) ? theme('localist_address', $wrapper->geo->value()) : '&lt;' . t('Not set') . '&gt;',
  );

  // Attach any extra fields.
  field_attach_form('localist_place', $localist_place, $build, $form_state);

  // Add the submit button.
  $build['submit'] = array(
    '#type' => 'submit',
    '#weight' => 100,
    '#value' => t('Save Localist Place'),
  );

  // Return the modified form.
  return $build;
}

/**
 * Form API submit callback to save the place form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state.
 */
function localist_place_form_submit(array $form, array &$form_state) {
  $localist_place = $form_state['build_info']['args'][0];
  $wrapper = entity_metadata_wrapper('localist_place', $localist_place);
  $values = (object) $form_state['values'];

  // Merge the consolidated objects.
  $localist_place = (object) array_merge((array) $localist_place, (array) $values);

  // Save the updated field to the database.
  field_attach_update('localist_place', $localist_place, $form, $form_state);

  // Alert the user that a save was successful.
  drupal_set_message(t('<em>!place_name</em> saved.', array('!place_name' => $wrapper->name->value()), PASS_THROUGH));

  // Redirect to the view.
  $form_state['redirect'] = 'localist/place/' . $wrapper->lpid->value();
}
