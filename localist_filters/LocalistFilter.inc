<?php
/**
 * @file
 * Defines the Localist Filter entity.
 */

/**
 * Localist Filter class.
 */
class LocalistFilter extends Entity {
  /**
   * Localist Filter ID.
   *
   * @var integer
   */
  public $id = 0;

  /**
   * Language constant.
   *
   * @var string
   */
  public $language = LANGUAGE_NONE;

  /**
   * Name of the filter.
   *
   * @var string
   */
  public $name = '';

  /**
   * Parent ID of the filter.
   *
   * @var integer
   */
  public $parent_id = 0;

  /**
   * Define the constructor, and set the parent & children filters.
   *
   * @param array $values
   *   The values.
   * @param string $bundle_type
   *   The name of the bundle.
   */
  public function __construct($values = array(), $bundle_type = NULL) {
    parent::__construct($values, $bundle_type);

    // Check if parent_id is NULL from the API.
    $values['parent_id'] = (empty($values['parent_id'])) ? 0 : $values['parent_id'];

    // If we're not creating a new node, do some conversions.
    if (isset($this->lfid)) {
      // Get the children and the parent.
      $this->children = $this->getChildren();
      $this->parent_id = $this->getParentId();
    }
  }

  /**
   * Displays the default label for the entity.
   *
   * @return string
   *   The label.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * The default URI for the entity.
   *
   * @return string
   *   The entity URI.
   */
  protected function defaultUri() {
    return array('path' => 'localist/filter/' . $this->identifier());
  }

  /**
   * Helper function to get the type label.
   *
   * @return string
   *   A string representing the filter type.
   */
  public function getTypeLabel() {
    $result = db_select('localist_filter_type', 'l')
      ->fields('l')
      ->condition('l.type', $this->type, '=')
      ->execute()
      ->fetchAssoc();
    return $result['label'];
  }

  /**
   * Returns an array of LocalistFilter objects representing children.
   *
   * @return array
   *   An array of lfids that are the children of the current LocalistFilter.
   */
  protected function getChildren() {
    // Grab the children.
    $result = db_select('localist_filter', 'b')
      ->fields('b')
      ->condition('b.parent_id', $this->id, '=')
      ->orderBy('b.name', 'asc')
      ->execute();

    // Set the children array.
    $children = array();
    while ($row = $result->fetchAssoc()) {
      $children[] = entity_load('localist_filter', array($row['lfid']));
    }

    return $children;
  }

  /**
   * Returns an integer representing the parent lfid of the current filter.
   *
   * @return int
   *   An lfid for the parent ID.
   */
  protected function getParentId() {
    // Grab the children.
    $result = db_select('localist_filter', 'b')
      ->fields('b')
      ->condition('b.id', $this->parent_id, '=')
      ->orderBy('b.name', 'asc')
      ->execute()
      ->fetchAssoc();

    // Return the parent.
    return $result['lfid'];
  }
}

/**
 * Localist Filter Controller.
 */
class LocalistFilterController extends EntityAPIController {
  /**
   * Builds the content for the Localist Event object.
   *
   * @param object $entity
   *   The Localist Event object.
   * @param string $view_mode
   *   The view mode to display.
   * @param string $langcode
   *   The language code to use.
   * @param array $content
   *   An array of content to display.
   *
   * @return array
   *   A render array of content.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Get a wrapper around the entity.
    $wrapper = entity_metadata_wrapper($this->entityType, $entity);

    // Attach the parent, if they exist.
    if (!empty($entity->parent_id)) {
      $parent = entity_metadata_wrapper('localist_filter', localist_filter_load($entity->parent_id));
      $content['parent_filter'] = array(
        '#title' => t('Parent Filter'),
        '#field_name' => 'parent_filter',
        '#field_type' => 'text',
        '#items' => array(array('value' => $parent->name->value())),
        0 => array(
          '#markup' => l($parent->name->value(), 'localist/filter/' . $parent->lfid->value()),
        ),
      );
    }

    // Attach the children to the array, if they exist.
    if (!empty($entity->children)) {
      $content['child_filters'] = array(
        '#title' => t('Child Filters'),
        '#field_name' => 'child_filters',
        '#field_type' => 'text',
        '#items' => array(),
      );
      foreach ($entity->children as $child) {
        // Grab a wrapper for this child entity.
        $child = entity_metadata_wrapper($this->entityType, array_values($child)[0]);

        // Set the items for each part of the field.
        $content['child_filters']['#items'][] = array(
          'value' => $child->name->value(),
        );
        $content['child_filters'][] = array(
          '#markup' => l($child->name->value(), 'localist/filter/' . $child->lfid->value()),
        );
      }
    }

    // Attach common parameters.
    $weight = count($content) * -1;
    list(, , $bundle) = entity_extract_ids($this->entityType, $entity);
    foreach ($content as &$item) {
      $item += array(
        '#theme' => 'field',
        '#weight' => $weight,
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => $wrapper->language->value(),
        '#bundle' => $bundle,
        '#entity_type' => $this->entityType,
      );

      // Increment the weight.
      $weight++;
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}
