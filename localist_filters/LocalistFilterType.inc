<?php
/**
 * @file
 * The definition of the Localist Filter Entity API objects.
 */

/**
 * LocalistFilterType class.
 */
class LocalistFilterType extends Entity {
  /**
   * The filter type.
   *
   * @var string
   */
  public $type;

  /**
   * The filter label.
   *
   * @var string
   */
  public $label;

  /**
   * The filter weight.
   *
   * @var integer
   */
  public $weight = 0;

  /**
   * The constructor function.
   *
   * @param array $values
   *   An array of values.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'localist_filter_type');
  }

  /**
   * Determine if the entity is locked for editing.
   *
   * @return bool
   *   Returns true if the entity is locked, or false if it's available for
   *   editing.
   */
  protected function isLocked() {
    // Check to see if this entity is locked or not.
    return isset($this->status) and empty($this->is_new) and (($this->status & ENTITY_IN_CODE) or ($this->status & ENTITY_FIXED));
  }
}

/**
 * LocalistFilterTypeController class.
 */
class LocalistFilterTypeController extends EntityAPIControllerExportable {
  /**
   * Override the create method.
   *
   * @param array $values
   *   An array of values.
   *
   * @return object
   *   The Localist Filter Type object.
   */
  public function create(array $values = array()) {
    return parent::create($values);
  }

  /**
   * Override the save method.
   *
   * @param object $entity
   *   The entity object.
   * @param DatabaseTransaction $transaction
   *   The database transaction.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // Check to make sure the entity doesn't exist before saving by checking the
    // remote Localist object ID saved in our database.
    $result = db_select($this->entityInfo['base table'], 'b')
      ->fields('b')
      ->condition('type', $entity->type, '=')
      ->execute()
      ->fetchAssoc();

    if (!empty($result)) {
      // Update.
      $updated_entity = entity_load($this->entityType, array($result['id']));
      $updated_entity = array_pop($updated_entity);

      // Adjust the possible new label.
      $updated_entity->label = $entity->label;

      // Update the type.
      parent::save($updated_entity, $transaction);
    }
    else {
      // New entity bundle.
      parent::save($entity, $transaction);
    }

    // Alert the system that we need to rebuild the system.
    // @see _http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }

}

/**
 * LocalistFilterTypeUIController class.
 */
class LocalistFilterTypeUIController extends LocalistObjectTypeUIController {
}
