<?php
/**
 * @file
 * Defines the filter admin form.
 */

/**
 * Form API callback to generate the filter edit form.
 *
 * @param array $form
 *   The Form API array.
 * @param array &$form_state
 *   The Form API form state array.
 * @param object $localist_filter
 *   The Localist Filter object.
 *
 * @return array
 *   The form.
 */
function localist_filter_form(array $form, array &$form_state, $localist_filter) {
  $build = array();
  $wrapper = entity_metadata_wrapper('localist_filter', $localist_filter);

  // Set the title.
  drupal_set_title(t('Edit <em>!filter_name</em>', array('!filter_name' => $wrapper->name->value())), PASS_THROUGH);

  // Set the un-editable field properties.
  $build['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $wrapper->name->value(),
  );

  // Set the parent filter.
  if (!empty($localist_filter->parent_id)) {
    $parent = entity_metadata_wrapper('localist_filter', localist_filter_load($localist_filter->parent_id));
    $build['parent'] = array(
      '#type' => 'item',
      '#title' => t('Parent Filter'),
      '#markup' => l($parent->name->value(), 'localist/filter/' . $parent->lfid->value() . '/edit'),
    );
  }

  // Set the children filters.
  if (!empty($localist_filter->children)) {
    $items = array();
    foreach ($localist_filter->children as $child) {
      $child = array_values($child)[0];
      $items[] = l($child->name, 'localist/filter/' . $child->lfid . '/edit');
    }

    $build['child_filters'] = array(
      '#type' => 'item',
      '#title' => t('Child Filters'),
      '#markup' => theme('item_list', array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array(),
      )),
    );
  }

  // Attach any extra fields.
  field_attach_form('localist_filter', $localist_filter, $build, $form_state);

  // Add the submit button.
  $build['submit'] = array(
    '#type' => 'submit',
    '#weight' => 100,
    '#value' => t('Save Localist Filter'),
  );

  // Return the modified form.
  return $build;
}

/**
 * Form API submit callback to save the filter form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state.
 */
function localist_filter_form_submit(array $form, array &$form_state) {
  $localist_filter = $form_state['build_info']['args'][0];
  $wrapper = entity_metadata_wrapper('localist_filter', $localist_filter);
  $values = (object) $form_state['values'];

  // Merge the consolidated objects.
  $localist_filter = (object) array_merge((array) $localist_filter, (array) $values);

  // Save the updated field to the database.
  field_attach_update('localist_filter', $localist_filter, $form, $form_state);

  // Alert the user that a save was successful.
  drupal_set_message(t('<em>!filter_name</em> saved.', array('!filter_name' => $wrapper->name->value(), PASS_THROUGH)));

  // Redirect to the view.
  $form_state['redirect'] = 'localist/filter/' . $wrapper->lfid->value();
}

/**
 * Helper function to handle the filter autocomplete.
 *
 * @param string $string
 *   The passed in value to autocomplete against.
 */
function _localist_integration_filter_autocomplete($string) {
  $matches = array();

  // If we have any input, start processing.
  if ($string) {
    // We need to support multiple filters, which can be treated as an AND or
    // an OR, depending on a user setting.
    $items = drupal_explode_tags($string);
    $last_item = array_pop($items);
    $prefix = implode(', ', $items);

    // Filter by Localist Filters.
    $result = db_select('localist_filter', 'lf')
      ->fields('lf', array('name'))
      ->condition('name', '%' . db_like($last_item) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();

    // Save the query to matches.
    foreach ($result as $row) {
      $value = (!empty($prefix)) ? $prefix . ', ' . $row->name : $row->name;
      $matches[$value] = check_plain($row->name);
    }
  }

  // Return the result to the form in JSON.
  drupal_json_output($matches);
}
