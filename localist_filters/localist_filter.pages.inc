<?php
/**
 * @file
 * Defines the filter view page.
 */

/**
 * Displays the localist filter.
 *
 * @param int $lfid
 *   An lfid to load.
 *
 * @return array
 *   A render array of the current filter.
 */
function localist_filter_view($lfid) {
  // Load the entity.
  $filter = entity_metadata_wrapper('localist_filter', $lfid);

  // Set the title.
  drupal_set_title($filter->name->value(), PASS_THROUGH);

  return $filter->view();
}
