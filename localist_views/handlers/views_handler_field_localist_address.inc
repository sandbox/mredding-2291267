<?php

/**
 * @file
 * Defines the view handler to display an address.
 */

/**
 * Creates a view handler to display address information.
 *
 * @ingroup views_handler_field
 */
class views_handler_field_localist_address extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($value = $this->get_value($values)) {
      return theme('localist_address', unserialize($value));
    }

    // Return null, since there's no data.
    return NULL;
  }
}
