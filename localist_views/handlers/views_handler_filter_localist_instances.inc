<?php
/**
 * @file
 * Views handler to filter against a start date.
 */

/**
 * Filter by localist instance start date.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_localist_instances extends views_handler_filter_date {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();

    // Add the event join to the query.
    $instance_data_join = new views_join();
    $instance_data_join->table = 'localist_event_instance_data';
    $instance_data_join->left_table = 'localist_event';
    $instance_data_join->field = 'event_leid';
    $instance_data_join->left_field = 'leid';
    $instance_data_join->type = 'INNER';
    $this->query->add_relationship('instance_data', $instance_data_join, 'localist_event_instance_data');

    // Attach the conditionals.
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}('instance_data.start');
    }
  }
}
