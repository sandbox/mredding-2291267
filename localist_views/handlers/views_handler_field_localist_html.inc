<?php
/**
 * @file
 * Defines a view handler to display the description in HTML.
 */

/**
 * Creates a view handler to display html entities.
 *
 * @ingroup views_handler_field
 */
class views_handler_field_localist_html extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function render($values) {
    return check_markup($this->get_value($values), 'localist_description');
  }
}
