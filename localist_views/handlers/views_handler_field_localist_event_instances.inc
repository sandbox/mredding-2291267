<?php
/**
 * @file
 * Display event instances on the page.
 */

/**
 * Creates a view handler to display event instances.
 *
 * @ingroup views_handler_field
 */
class views_handler_field_localist_event_instances extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['date_format'] = array('default' => 'small');
    $options['custom_date_format'] = array('default' => '');
    $options['timezone'] = array('default' => '');
    $options['group_dates'] = array('default' => FALSE);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    $date_formats = array();
    $date_types = system_get_date_types();
    foreach ($date_types as $value) {
      $date_formats[$value['type']] = t('@date_format format', array('@date_format' => $value['title'])) . ': ' . format_date(REQUEST_TIME, $value['type']);
    }

    $form['group_dates'] = array(
      '#type' => 'checkbox',
      '#title' => t('Group Dates'),
      '#description' => t('If checked, the dates will appear as a range in the format specified. If unchecked, the dates will appear as an ordered list starting with the most recent.'),
      '#default_value' => isset($this->options['group_dates']) ? $this->options['group_dates'] : FALSE,
    );
    $form['date_format'] = array(
      '#type' => 'select',
      '#title' => t('Date format'),
      '#options' => $date_formats + array(
        'custom' => t('Custom'),
      ),
      '#default_value' => (isset($this->options['date_format'])) ? $this->options['date_format'] : 'small',
    );
    $form['custom_date_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom date format'),
      '#description' => t('If "Custom", see the <a href="@url" target="_blank">PHP manual</a> for date formats. Otherwise, enter the number of different time units to display, which defaults to 2.', array('@url' => 'http://php.net/manual/function.date.php')),
      '#default_value' => isset($this->options['custom_date_format']) ? $this->options['custom_date_format'] : '',
      '#dependency' => array(
        'edit-options-date-format' => array(
          'custom',
          'raw time ago',
          'time ago',
          'today time ago',
          'raw time hence',
          'time hence',
          'raw time span',
          'time span',
          'raw time span',
          'inverse time span',
          'time span',
        ),
      ),
    );
    $form['timezone'] = array(
      '#type' => 'select',
      '#title' => t('Timezone'),
      '#description' => t('Timezone to be used for date output.'),
      '#options' => array('' => t('- Default site/user timezone -')) + system_time_zones(FALSE),
      '#default_value' => $this->options['timezone'],
      '#dependency' => array('edit-options-date-format' => array_merge(array('custom'), array_keys($date_formats))),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();

    // Attach the event leid for use in the render.
    $this->query->add_field('localist_event', 'leid', 'localist_event_leid');
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // Get the date format.
    $date_options = array(
      'date_format' => $this->options['date_format'],
      'custom_date_format' => $this->options['custom_date_format'],
      'timezone' => $this->options['timezone'],
    );

    // Get the instances.
    $query = db_select('localist_event_instance_data', 'instance_data');
    $query->fields('instance_data');
    $query->condition('instance_data.event_leid', $values->localist_event_leid);
    $query->orderBy('start', 'ASC');
    $result = $query->execute();
    $instances = array();
    while ($row = $result->fetchAssoc()) {
      $instances[] = $row;
    }

    // If we're grouping dates, go ahead and group them.
    $return = array();
    if ($this->options['group_dates']) {
      $start = $instances[0];
      $end = $instances[count($instances) - 1];
      $start['end'] = (!empty($end['end'])) ? $end['end'] : $end['start'];
      $return = array(
        '#markup' => theme('localist_event_instance', array('instance' => $start, 'options' => $date_options)),
      );
    }
    // Otherwise, display the dates.
    else {
      foreach ($instances as $instance) {
        $return[] = array(
          '#markup' => theme('localist_event_instance', array(
            'instance' => $instance,
            'options' => $date_options,
          )),
        );
      }
    }

    return $return;
  }
}
