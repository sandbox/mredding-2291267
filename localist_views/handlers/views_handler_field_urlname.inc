<?php
/**
 * @file
 * The views handler to generate a remote URL to the calendar event.
 */

/**
 * Define a views handler to display a URL field.
 *
 * @ingroup views_handler_field
 */
class views_handler_field_urlname extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['cta_caption'] = array('default' => 'Learn More');

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    $form['cta_caption'] = array(
      '#type' => 'textfield',
      '#title' => t('Call to action caption'),
      '#description' => t('Choose a call to action caption.'),
      '#default_value' => isset($this->options['cta_caption']) ? $this->options['cta_caption'] : '',
    );
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($url = $this->get_value($values)) {
      return l(
        isset($this->options['cta_caption']) ? $this->options['cta_caption'] : t('Learn More'),
        variable_get('localist_organization_url', '') . 'event/' . $url,
        array('external' => TRUE, 'attributes' => array('target' => '_blank'))
      );
    }

    return NULL;
  }
}
