<?php
/**
 * @file
 * Defines a view handler to render remote photos.
 */

/**
 * Creates a view handler for displaying remote URLs as rendered images.
 *
 * @ingroup views_handler_field
 */
class views_handler_field_localist_image extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // Convert the value into an image.
    return theme('image', array(
      'path' => $this->get_value($values),
      'width' => NULL,
      'height' => NULL,
      'alt' => NULL,
      'attributes' => array(
        'class' => array('localist-photo', 'localist-location-photo'),
      ),
    ));
  }
}
