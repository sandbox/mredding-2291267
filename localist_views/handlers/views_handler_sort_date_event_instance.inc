<?php
/**
 * @file
 * Defines a sort handler to sort by event instance start date.
 */

/**
 * Defines a sort handler to sort by date.
 *
 * @ingroup views_handler_sort
 */
class views_handler_sort_date_event_instance extends views_handler_sort_date {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();

    $table = 'localist_event_instance_data';
    $left_table = $this->table;
    $left_field = 'leid';
    $field = 'event_leid';
    $join = new views_join();
    $join->construct($table, $left_table, $left_field, $field, array(), 'INNER');

    $alias = 'event_instance_sort_join';
    $alias = $this->query->add_relationship($alias, $join, $this->table_alias, $this->relationship);

    switch ($this->options['granularity']) {
      case 'second':
      default:
        $this->query->add_orderby($alias, 'start', $this->options['order']);
        return;

      case 'minute':
        $formula = views_date_sql_format('YmdHi', $alias . '.start');
        break;

      case 'hour':
        $formula = views_date_sql_format('YmdH', $alias . '.start');
        break;

      case 'day':
        $formula = views_date_sql_format('Ymd', $alias . '.start');
        break;

      case 'month':
        $formula = views_date_sql_format('Ym', $alias . '.start');
        break;

      case 'year':
        $formula = views_date_sql_format('Y', $alias . '.start');
        break;
    }

    // Add the field.
    $this->query->add_orderby(NULL, $formula, $this->options['order'], $alias . '_start_' . $this->options['granularity']);
  }
}
