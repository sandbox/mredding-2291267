<?php
/**
 * @file
 * Defines a handler to filter against all day events.
 */

/**
 * Define a handler for all day events.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_all_day_event extends views_handler_filter_boolean_operator {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();

    // Add the event join to the query.
    $instance_data_join = new views_join();
    $instance_data_join->table = 'localist_event_instance_data';
    $instance_data_join->left_table = 'localist_event';
    $instance_data_join->field = 'event_leid';
    $instance_data_join->left_field = 'leid';
    $instance_data_join->type = 'INNER';
    $this->query->add_relationship('instance_data', $instance_data_join, 'localist_event_instance_data');

    // Attach field logic.
    $this->query->add_where('instance_data', 'instance_data.all_day', $this->value, '=');
  }
}
