<?php
/**
 * @file
 * Views handler to add filter by a Localist Filter entity logic.
 */

/**
 * Filter by filter id.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_localist_filter extends views_handler_filter {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['operator']['default'] = 'or';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = array(
      'or' => array(
        'title' => t('Is one of'),
        'short' => t('or'),
        'short_single' => t('='),
        'method' => 'op_helper',
        'values' => 1,
        'ensure_my_table' => 'helper',
      ),
      'and' => array(
        'title' => t('Is all of'),
        'short' => t('and'),
        'short_single' => t('='),
        'method' => 'op_helper',
        'values' => 1,
        'ensure_my_table' => 'helper',
      ),
    );

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    $operators = $this->operators();

    // Set up the options array.
    $options = array();
    foreach ($operators as $key => $operator) {
      $options[$key] = $operator['title'];
    }

    // Define the form.
    $form['operator'] = array(
      '#type' => 'radios',
      '#title' => t('Filter type'),
      '#default_value' => $this->options['operator'],
      '#options' => $options,
    );

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Localist Filter'),
      '#default_value' => $this->get_filter_string($this->value),
      '#autocomplete_path' => 'localist/filter/autocomplete',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function value_validate($form, &$form_state) {
    $values = drupal_explode_tags($form_state['values']['options']['value']);
    $lfids = $this->validate_filter_strings($form['value'], $values);

    if ($lfids) {
      $form_state['values']['options']['value'] = $lfids;
    }
  }

  /**
   * Helper function to generate a string of value filters.
   *
   * @param mixed $filters
   *   Empty string OR array of lfids.
   *
   * @return string
   *   A string representing the filters.
   */
  public function get_filter_string($filters) {
    // If the $filters variable has content, let's start parsing out what the
    // actual values of the string are.
    if (!empty($filters)) {
      $tmp = array();
      $filters = localist_filter_load_multiple($filters);
      foreach ($filters as $filter) {
        $tmp[] = $filter->name;
      }

      // Return the formatted string.
      return implode(', ', $tmp);
    }

    // Base case is an empty string.
    return '';
  }

  /**
   * Validate the user string. Abstracted for future use.
   *
   * @param array $form
   *   The form which is used, either the views ui or the exposed filters.
   * @param array $values
   *   The LocalistFilter names which will be converted to tids.
   *
   * @return array
   *   The localist filter lfids fo all validated filters.
   */
  public function validate_filter_strings(array &$form, array $values) {
    if (empty($values)) {
      return array();
    }

    $lfids = array();
    $names = array();
    $missing = array();
    foreach ($values as $value) {
      $missing[strtolower($value)] = TRUE;
      $names[] = $value;
    }

    if (!$names) {
      return FALSE;
    }

    // Find the filtered term names and return the LFID.
    $query = db_select('localist_filter', 'lf');
    $query->fields('lf');
    $query->condition('lf.name', $names);
    $result = $query->execute();

    foreach ($result as $filter) {
      unset($missing[strtolower($filter->name)]);
      $lfids[] = (int) $filter->lfid;
    }

    if ($missing && !empty($this->options['error_message'])) {
      form_error($form, format_plural(count($missing), 'Unable to find term: @filters', 'Unable to find terms: @filters', array('@filters' => implode(', ', array_keys($missing)))));
    }
    elseif ($missing && empty($this->options['error_message'])) {
      $lfids = array(0);
    }

    return $lfids;
  }

  /**
   * {@inheritdoc}
   */
  public function value_submit($form, &$form_state) {
    // Prevent array_filter from messing up our arrays in parent submit.
  }

  /**
   * {@inheritdoc}
   */
  public function admin_summary() {
    $op = array(
      'and' => 'all of',
      'or' => 'one of',
      'not' => 'none of',
    );

    // Return the operator and the terms.
    return check_plain($op[$this->operator]) . ' ' . check_plain($this->get_filter_string($this->value));
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();

    // Add the event join to the query.
    $filter_data_join = new views_join();
    $filter_data_join->table = 'localist_event_filter_data';
    $filter_data_join->left_table = 'localist_event';
    $filter_data_join->field = 'event_leid';
    $filter_data_join->left_field = 'leid';
    $filter_data_join->type = 'INNER';
    $this->query->add_relationship('lefd', $filter_data_join, 'localist_event_filter_data');

    // Attach conditionals.
    $this->attach_conditionals($this->value, $this->operator);
  }

  /**
   * Helper function to attach conditionals to the query.
   *
   * @param mixed $values
   *   Either an empty string or an array of LocalistFilter lfids.
   * @param string $operator
   *   A string denoting the operation to attach filters.
   */
  public function attach_conditionals($values, $operator) {
    // Only attach conditionals if we have any.
    if (count($values)) {
      // Figure out the operator.
      switch ($operator) {
        case 'and':
          // Must match all filters.
          $condition = db_and();
          foreach ($values as $value) {
            $condition->condition('lefd.filter_lfid', $value, '=');
          }
          $this->query->add_where('filter_lfid', $condition);
          break;

        case 'or':
          // Must match any filters. Use IN.
          $this->query->add_where('filter_lfid', 'lefd.filter_lfid', $values, 'IN');
          break;
      }
    }
  }
}
