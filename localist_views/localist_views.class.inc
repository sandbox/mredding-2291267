<?php
/**
 * @file
 * Defines basic class handlers for the localist_views module.
 */

/**
 * Set up handlers to automatically add all entity properties to the filters.
 */
class LocalistFilterViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }
}

/**
 * Set up handlers to automatically add all entity properties to the places.
 */
class LocalistPlaceViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();
    $settings = &$data['localist_place'];

    // Strip out fields we don't want the user to use in a field definition.
    unset($settings['id']['field']);
    unset($settings['type']['field']);
    unset($settings['language']['field']);
    unset($settings['urlname']['field']);

    // Adjust title information.
    $settings['lpid']['title'] = t('Entity ID');
    $settings['id']['title'] = t('Object ID');
    $settings['type']['title'] = t('Entity Type');
    $settings['name']['title'] = t('Name');
    $settings['description']['title'] = t('Description');
    $settings['place_type']['title'] = t('Type');
    $settings['urlname']['title'] = t('URL Slug');
    $settings['photo_url']['title'] = t('Photo');

    // Adjust help information.
    $settings['lpid']['help'] = t('The unique ID for the Localist Place.');
    $settings['id']['help'] = t('The Localist ID for the Localist Place.');
    $settings['type']['help'] = t('The Localist Place entity type.');
    $settings['language']['help'] = t('The Localist Place language machine name.');
    $settings['name']['help'] = t('The name of the Localist Place.');
    $settings['description']['help'] = t('The description of the Localist Place.');
    $settings['place_type']['help'] = t('The type of Localist Place.');
    $settings['urlname']['help'] = t('The remote Localist url slug.');
    $settings['photo_url']['help'] = t('The remote Localist photo.');

    // Adjust field render handlers.
    $settings['name']['field']['handler'] = 'views_handler_field_localist_html';
    $settings['description']['field']['handler'] = 'views_handler_field_localist_html';
    $settings['photo_url']['field']['handler'] = 'views_handler_field_localist_image';

    // Create address field.
    $settings['address'] = array(
      'title' => t('Address'),
      'help' => t('The address of the Localist Place.'),
      'field' => array(
        'real field' => 'geo',
        'handler' => 'views_handler_field_localist_address',
        'click sortable' => FALSE,
      ),
    );

    return $data;
  }
}

/**
 * Set up handlers to automatically add all entity properties to the events.
 */
class LocalistEventViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();
    $settings = &$data['localist_event'];

    // Strip out fields we don't want the user to use in a field definition.
    unset($settings['id']['field']);
    unset($settings['type']['field']);
    unset($settings['language']['field']);

    // Adjust title information.
    $settings['leid']['title'] = t('Entity ID');
    $settings['id']['title'] = t('Object ID');
    $settings['type']['title'] = t('Entity Type');
    $settings['name']['title'] = t('Name');
    $settings['description']['title'] = t('Description');
    $settings['place_type']['title'] = t('Type');
    $settings['urlname']['title'] = t('Localist URL');
    $settings['photo_url']['title'] = t('Photo');

    // Adjust help information.
    $settings['lpid']['help'] = t('The unique ID for the Localist Event.');
    $settings['id']['help'] = t('The Localist ID for the Localist Event.');
    $settings['type']['help'] = t('The Localist Event entity type.');
    $settings['language']['help'] = t('The Localist Event language machine name.');
    $settings['name']['help'] = t('The name of the Localist Event.');
    $settings['description']['help'] = t('The description of the Localist Event.');
    $settings['place_type']['help'] = t('The type of Localist Event.');
    $settings['urlname']['help'] = t('The URL to the Localist calendar event.');
    $settings['photo_url']['help'] = t('The remote Localist photo.');

    // Adjust the render handler for url name.
    $settings['urlname']['field']['handler'] = 'views_handler_field_urlname';

    // Create instances field.
    $settings['instances'] = array(
      'title' => t('Event Instances'),
      'help' => t('The instances of the event.'),
      'field' => array(
        'real field' => 'instances',
        'handler' => 'views_handler_field_localist_event_instances',
        'click sortable' => FALSE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_localist_instances',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date_event_instance',
      ),
    );

    // Create a Filter filter.
    $settings['localist_filter'] = array(
      'title' => t('Localist Filters'),
      'help' => t('Localist Filters for the Localist Event.'),
      'filter' => array(
        'handler' => 'views_handler_localist_filter',
      ),
    );

    // Create all day event filter.
    $settings['all_day_event'] = array(
      'title' => t('All Day Event'),
      'help' => t('Checks to see if an event is an all day event or not.'),
      'filter' => array(
        'handler' => 'views_handler_filter_all_day_event',
      ),
    );

    // Adjust field render handlers.
    $settings['name']['field']['handler'] = 'views_handler_field_localist_html';
    $settings['description']['field']['handler'] = 'views_handler_field_localist_html';
    $settings['photo_url']['field']['handler'] = 'views_handler_field_localist_image';

    // If there's a relationship set, adjust the relationship.
    if (isset($data['localist_place']['localist_event']['relationship'])) {
      // Make sure that the relationship field checks on ID and not lpid. The
      // relationship is not sanitized to use the lpid field.
      $data['localist_place']['localist_event']['relationship']['relationship field'] = 'id';
    }
    if (isset($data['localist_event']['place_id']['relationship'])) {
      // Make sure the other direction is set, as well.
      $data['localist_event']['place_id']['relationship']['base field'] = 'id';
    }

    return $data;
  }

}
