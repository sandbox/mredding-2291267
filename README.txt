INTRODUCTION
------------
The Localist Integration module provides two modules: Localist Integration,
which provides a sync between your local Drupal site and your Localist system,
and the Localist Views module, which provides rudimentary views information.

INSTALLATION
------------
1. Unpack the localist_integration folder and contents in the appropriate
   modules directory of your Drupal installation.

2. Enable the localist_integration and localist_views modules on the Modules
   admin page.

CONFIGURATION
-------------
1. Go to the module configuration screen and paste in your Localist API
   endpoint.

2. Set a number of days to sync. Recommended to only use 45 days.

NOTICE
-------------
This module is provided as beta release. Please report all issues to the
module's issue queue.
