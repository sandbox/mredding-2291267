<?php
/**
 * @file
 * Handles the Localist Event view page.
 */

/**
 * Displays a Localist Event.
 *
 * @param int $leid
 *   The entity ID for the Localist Event.
 *
 * @return array
 *   Render array representing the Localist Event view.
 */
function localist_event_view($leid) {
  // Load the entity.
  $event = entity_metadata_wrapper('localist_event', $leid);

  // Set the title.
  drupal_set_title($event->name->value(), PASS_THROUGH);

  return $event->view();
}
