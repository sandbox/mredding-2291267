<?php
/**
 * @file
 * The Localist Event class.
 */

/**
 * Localist Event class.
 */
class LocalistEvent extends Entity {

  /**
   * The ID.
   *
   * @var integer
   */
  public $id = 0;

  /**
   * The event language.
   *
   * @var string
   */
  public $language = LANGUAGE_NONE;

  /**
   * The event name.
   *
   * @var string
   */
  public $name = '';

  /**
   * The constructor.
   *
   * @param array $values
   *   An array of entities to create.
   * @param string $bundle_type
   *   The bundle of the object.
   */
  public function __construct($values = array(), $bundle_type = NULL) {
    parent::__construct($values, $bundle_type);

    // Set the new place ID.
    $this->place_id = $this->getPlaceId();

    // Set the new filters.
    $this->filters = $this->getFilters();

    // Set the instances.
    $this->instances = $this->getInstances();
  }

  /**
   * Displays the default label for the entity.
   *
   * @return string
   *   The label.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * The default URI for the entity.
   *
   * @return string
   *   The entity URI.
   */
  protected function defaultUri() {
    return array('path' => 'localist/event/' . $this->identifier());
  }

  /**
   * Returns an integer representing the actual place ID.
   *
   * @return int
   *   A Localist Place lpid.
   */
  protected function getPlaceId() {
    // Grab the children.
    $result = db_select('localist_place', 'b')
      ->fields('b')
      ->condition('b.id', $this->place_id, '=')
      ->orderBy('b.name', 'asc')
      ->execute()
      ->fetchAssoc();

    // Return the parent.
    return $result['lpid'];
  }

  /**
   * Looks at the join table of localist filter data to return the filter IDs.
   *
   * @return array
   *   An array of LocalistFilter IDs.
   */
  protected function getFilters() {
    $return = array();

    // Get the set of filters.
    $filters = db_query('SELECT lefd.filter_lfid FROM {localist_event_filter_data} lefd
      WHERE lefd.event_leid = :event_id
      GROUP BY lefd.filter_lfid', array(':event_id' => $this->leid));

    while ($row = $filters->fetchAssoc()) {
      $return[] = $row['filter_lfid'];
    }

    // Return an array of LocalistFilter IDs.
    return $return;
  }

  /**
   * Attaches the instances of the event.
   *
   * @return array
   *   Returns an array of instance data.
   */
  protected function getInstances() {
    $instances = array();

    // Grab the instance objects.
    $query = db_query('SELECT * FROM {localist_event_instance_data} l
      INNER JOIN {localist_event} le ON le.leid = l.event_leid
      WHERE l.event_leid = :event_id GROUP BY l.instance_id', array(':event_id' => $this->leid));

    while ($row = $query->fetchAssoc()) {
      $instances[] = $row;
    }

    return $instances;
  }

}

/**
 * Localist Event Controller.
 */
class LocalistEventController extends EntityAPIController {
  /**
   * Builds the content for the Localist Event object.
   *
   * @param object $entity
   *   The Localist Event object.
   * @param string $view_mode
   *   The view mode to display.
   * @param string $langcode
   *   The language code to use.
   * @param array $content
   *   An array of content to display.
   *
   * @return array
   *   A render array of content.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Get a wrapper around the entity.
    $wrapper = entity_metadata_wrapper($this->entityType, $entity);

    // Attach place description.
    if (!empty($entity->description)) {
      $content['description'] = array(
        '#title' => t('Description'),
        '#field_name' => 'description',
        '#field_type' => 'text_long',
        '#items' => array(array('value' => $wrapper->description->value())),
        0 => array(
          '#markup' => check_markup($wrapper->description->value(), 'localist_description'),
        ),
      );
    }

    // Attach instances.
    if (count($entity->instances) > 0) {
      $content['instances'] = array(
        '#title' => format_plural(count($entity->instances), 'Event Date', 'Event Dates'),
        '#field_name' => 'instances',
        '#field_type' => 'text',
        '#items' => array(),
      );
      foreach ($wrapper->instances->value() as $instance) {
        $content['instances']['#items'][] = array('value' => $instance['start']);
        $content['instances'][] = array(
          '#markup' => theme('localist_event_instance', $instance),
        );
      }
    }

    // Attach the filters to the event.
    if ($filters = $wrapper->filters->value()) {
      // Attach filters.
      $content['filters'] = array(
        '#title' => t('Filters'),
        '#field_name' => 'filters',
        '#field_type' => 'text',
        '#items' => array(),
      );
      foreach ($filters as $filter) {
        // Grab a wrapper for this child entity.
        $filter_wrapper = entity_metadata_wrapper('localist_filter', $filter);

        // Set the items for each part of the field.
        $content['filters']['#items'][] = array(
          'value' => $filter_wrapper->name->value(),
        );
        $content['filters'][] = array(
          '#markup' => l($filter_wrapper->name->value(), 'localist/filter/' . $filter_wrapper->lfid->value(), array('html' => TRUE)),
        );
      }
    }

    // Attach a location.
    if (!empty($entity->place_id)) {
      $content['location'] = array(
        '#title' => t('Location'),
        '#field_name' => 'location',
        '#field_type' => 'text',
        '#items' => array(array('value' => $wrapper->place_id->name->value())),
        0 => array(
          '#markup' => l(
            $wrapper->place_id->name->value(),
            'localist/place/' . $wrapper->place_id->lpid->value(),
            array('html' => TRUE))
          . ' ' . $wrapper->room_number->value(),
        ),
      );
    }

    // Attach photo.
    if (!empty($entity->photo_url)) {
      $content['photo'] = array(
        '#title' => t('Photo'),
        '#field_name' => 'photo',
        '#field_type' => 'image',
        '#items' => array(array('value' => $wrapper->photo_url->value())),
        0 => array(
          '#markup' => theme('image', array(
            'path' => $wrapper->photo_url->value(),
            'width' => NULL,
            'height' => NULL,
            'alt' => $wrapper->name->value(),
            'attributes' => array(
              'class' => array('localist-photo', 'localist-event-photo'),
            ),
          )),
        ),
      );
    }

    // Attach common parameters.
    $weight = count($content) * -1;
    list(, , $bundle) = entity_extract_ids($this->entityType, $entity);
    foreach ($content as &$item) {
      $item += array(
        '#theme' => 'field',
        '#weight' => $weight,
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => $wrapper->language->value(),
        '#bundle' => $bundle,
        '#entity_type' => $this->entityType,
      );

      // Increment the weight.
      $weight++;
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}
