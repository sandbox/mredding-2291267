<?php
/**
 * @file
 * Handles the batch uploads for the Localist Events.
 */

/**
 * Batch API callback to handle importing Localist Events.
 *
 * @param int $current_page
 *   The current page of the batch upload.
 * @param int $total_pages
 *   The total number of pages in the batch upload.
 * @param int $per_page
 *   The number of events to process per page.
 * @param object $localist_api
 *   The Localist API object.
 * @param array &$context
 *   The Batch API context array.
 */
function localist_integration_process_events($current_page, $total_pages, $per_page, $localist_api, array &$context) {
  // Set up the sandbox.
  if (!isset($context['sandbox']['current_page'])) {
    $context['sandbox']['current_page'] = $current_page;
    $context['sandbox']['total_pages'] = $total_pages;
    $context['sandbox']['per_page'] = $per_page;
    $context['sandbox']['current_item'] = 0;
    $context['sandbox']['localist_api'] = $localist_api;
  }

  // Grab the resultset.
  $events = $context['sandbox']['localist_api']->getEvents($context['sandbox']['current_page']);

  // Save each part of the resultset.
  foreach ($events as $event) {
    // First of all, the API returns weird results at times. See if the event
    // already exists because this may actually just be a new instance of the
    // event.
    $entity = localist_event_load_by_id($event['event']['id']);
    if (!$entity) {
      // This entity doesn't exist yet, so let's go ahead and create a new one
      // and save.
      $entity = new stdClass();
      $entity->id = $event['event']['id'];
      $entity->type = 'generic_event';
      $entity->language = LANGUAGE_NONE;
    }

    // Now it's safe to assign new items. This allows us to not duplicate our
    // entity.
    $entity->name = check_plain($event['event']['title']);
    $entity->description = check_plain($event['event']['description_text']);
    $entity->urlname = $event['event']['urlname'];
    $entity->photo_url = $event['event']['photo_url'];
    $entity->place_id = $event['event']['venue_id'];
    $entity->created = strtotime($event['event']['created_at']);
    $entity->updated = strtotime($event['event']['updated_at']);
    $entity->original = $event['event'];

    // At this point, let's go ahead and save the entity. A new event will get
    // an leid, but an old one will use the historic one.
    entity_get_controller('localist_event')->save($entity);

    // Parse out any filters. We need to do this after the entity update in case
    // filters for the event have changed.
    $filters = _localist_integration_event_filter_parser($event['event']['filters']);

    // Save the filters to the database.
    foreach ($filters as $filter) {
      // Get the local filter ID.
      $filter_lfid = db_select('localist_filter', 'lf')
        ->fields('lf', array('lfid'))
        ->condition('id', $filter, '=')
        ->execute()
        ->fetchAssoc();
      $filter_lfid = $filter_lfid['lfid'];

      // Insert the filter data.
      $filter_insert = db_insert('localist_event_filter_data')
        ->fields(array(
          'event_leid' => $entity->leid,
          'filter_lfid' => $filter_lfid,
        ))
        ->execute();
    }

    // Get the instances array.
    $instances = _localist_integration_event_instance_parser($event['event']['event_instances']);

    // Save the instances to the database.
    foreach ($instances as $instance) {
      // Insert the filter data.
      $instance_insert = db_insert('localist_event_instance_data')
        ->fields(array(
          'instance_id' => $instance['instance_id'],
          'event_leid' => $entity->leid,
          'start' => $instance['start'],
          'end' => $instance['end'],
          'all_day' => ($instance['all_day'] == 'false') ? 0 : 1,
        ))
        ->execute();
    }

    // Attach the name in the resultset.
    $context['results'][] = check_plain($entity->name);

    // Update the progress.
    $context['sandbox']['current_item'] = $entity->id;
    $context['message'] = t('Now processing @event.', array('@event' => $entity->name));
  }

  // Increment the current page.
  $context['sandbox']['current_page']++;
  if ($context['sandbox']['current_page'] <= $context['sandbox']['total_pages']) {
    $context['finished'] = $context['sandbox']['current_page'] / ($context['sandbox']['total_pages'] + 1);
  }
}

/**
 * The Batch API finished callback.
 *
 * @param bool $success
 *   Whether or not the batch API completed successfully.
 * @param array $results
 *   An array of results.
 * @param array $operations
 *   An array of operations.
 */
function localist_integration_process_events_finished($success, array $results, array $operations) {
  if ($success) {
    // Get a new message.
    $message = count($results) . ' events processed.';
    drupal_set_message($message);
  }
  else {
    // Unspecified error.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1],
    TRUE)));

    drupal_set_message($message, 'error');
  }
}

/**
 * Helper function to convert an event_instance array into a keyed array.
 *
 * @param array $instances
 *   The Localist API returned instance object.
 *
 * @return array
 *   Returns an array of instances.
 */
function _localist_integration_event_instance_parser(array $instances) {
  $return = array();
  foreach ($instances as $instance) {
    $return[] = array(
      'instance_id' => $instance['event_instance']['id'],
      'event_id' => $instance['event_instance']['event_id'],
      'start' => (!is_null($instance['event_instance']['start'])) ? strtotime($instance['event_instance']['start']) : 0,
      'end' => (!is_null($instance['event_instance']['end'])) ? strtotime($instance['event_instance']['end']) : 0,
      'all_day' => ((bool) $instance['event_instance']['all_day'] === TRUE) ? 1 : 0,
    );
  }

  return $return;
}

/**
 * Helper function to convert a filter array into a regular keyed array.
 *
 * @param array $filters
 *   An array of filters.
 *
 * @return array
 *   An array of keyed filters.
 */
function _localist_integration_event_filter_parser(array $filters) {
  $return = array();
  foreach ($filters as $filter_type) {
    foreach ($filter_type as $filter) {
      // We just want the array of filter IDs.
      $return[] = $filter['id'];
    }
  }

  return $return;
}
