<?php
/**
 * @file
 * The definition of the Localist Event Entity API objects.
 */

/**
 * LocalistEventType class.
 */
class LocalistEventType extends Entity {

  /**
   * Type variable.
   *
   * @var string
   */
  public $type;

  /**
   * Label variable.
   *
   * @var string
   */
  public $label;

  /**
   * Type weight.
   *
   * @var integer
   */
  public $weight = 0;

  /**
   * Override default constructor.
   *
   * @param array $values
   *   An array of values.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'localist_event_type');
  }

  /**
   * Determine if the entity is locked.
   *
   * @return bool
   *   Returns true if the entity is locked, or false if it is not.
   */
  protected function isLocked() {
    return isset($this->status) and empty($this->is_new) and (($this->status & ENTITY_IN_CODE) or ($this->status & ENTITY_FIXED));
  }

}

/**
 * LocalistEventTypeController class.
 */
class LocalistEventTypeController extends EntityAPIControllerExportable {
  /**
   * Override the create method.
   *
   * @param array $values
   *   An array of values.
   *
   * @return object
   *   The Localist Event Type object.
   */
  public function create(array $values = array()) {
    return parent::create($values);
  }

  /**
   * Override the save method.
   *
   * @param object $entity
   *   The entity object.
   * @param DatabaseTransaction $transaction
   *   The database transaction.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);

    // Alert the system that we need to rebuild the menu.
    // @see _http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }

}

/**
 * LocalistEventTypeUIController class.
 */
class LocalistEventTypeUIController extends LocalistObjectTypeUIController {
}
