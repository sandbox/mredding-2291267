<?php
/**
 * @file
 * Administration actions for Localist Events.
 */

/**
 * Form API callback to generate the event edit form.
 *
 * @param array $form
 *   The Form API array.
 * @param array &$form_state
 *   The Form API form state array.
 * @param object $localist_event
 *   The Localist Event object.
 *
 * @return array
 *   The form.
 */
function localist_event_form(array $form, array &$form_state, $localist_event) {
  $build = array();
  $wrapper = entity_metadata_wrapper('localist_event', $localist_event);

  // Set the title.
  drupal_set_title(t('Edit <em>!event_name</em>', array('!event_name' => $wrapper->name->value())), PASS_THROUGH);

  // Set the un-editable field properties.
  $build['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $wrapper->name->value(),
  );

  // Attach the description.
  if (!empty($localist_event->description)) {
    $build['description'] = array(
      '#type' => 'item',
      '#title' => t('Description'),
      0 => array(
        '#markup' => check_markup($wrapper->description->value(), 'localist_description'),
      ),
    );
  }

  // Attach instances.
  if (count($localist_event->instances) > 0) {
    $build['instances'] = array(
      '#type' => 'item',
      '#title' => format_plural(count($localist_event->instances), 'Event Date', 'Event Dates'),
    );
    foreach ($wrapper->instances->value() as $instance) {
      $build['instances'][] = array(
        '#markup' => theme('localist_event_instance', $instance),
      );
    }
  }

  // Attach the filters to the event.
  if ($filters = $wrapper->filters->value()) {
    // Attach filters.
    $build['filters'] = array(
      '#type' => 'item',
      '#title' => t('Filters'),
    );
    foreach ($filters as $filter) {
      // Grab a wrapper for this child entity.
      $filter_wrapper = entity_metadata_wrapper('localist_filter', $filter);

      // Set the items for each part of the field.
      $items[] = l($filter_wrapper->name->value(), 'localist/filter/' . $filter_wrapper->lfid->value(), array('html' => TRUE));
      $build['filters'][0] = array(
        '#markup' => theme('item_list', array(
          'items' => $items,
          'title' => NULL,
          'type' => 'ul',
          'attributes' => array(),
        )),
      );
    }
  }

  // Attach a location.
  if (!empty($localist_event->place_id)) {
    $build['location'] = array(
      '#type' => 'item',
      '#title' => t('Location'),
      0 => array(
        '#markup' => l(
          $wrapper->place_id->name->value(),
          'localist/place/' . $wrapper->place_id->lpid->value(),
          array('html' => TRUE))
        . ' ' . $wrapper->room_number->value(),
      ),
    );
  }

  // Attach photo.
  if (!empty($localist_event->photo_url)) {
    $build['photo'] = array(
      '#type' => 'item',
      '#title' => t('Photo'),
      0 => array(
        '#markup' => theme('image', array(
          'path' => $wrapper->photo_url->value(),
          'width' => NULL,
          'height' => NULL,
          'alt' => $wrapper->name->value(),
          'attributes' => array(
            'class' => array('localist-photo', 'localist-event-photo'),
          ),
        )),
      ),
    );
  }

  // Attach any extra fields.
  field_attach_form('localist_event', $localist_event, $build, $form_state);

  // Add the submit button.
  $build['submit'] = array(
    '#type' => 'submit',
    '#weight' => 100,
    '#value' => t('Save Localist Event'),
  );

  // Return the modified form.
  return $build;
}

/**
 * Form API submit callback to save the event form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state.
 */
function localist_event_form_submit(array $form, array &$form_state) {
  $localist_event = $form_state['build_info']['args'][0];
  $wrapper = entity_metadata_wrapper('localist_event', $localist_event);
  $values = (object) $form_state['values'];

  // Merge the consolidated objects.
  $localist_event = (object) array_merge((array) $localist_event, (array) $values);

  // Save the updated field to the database.
  field_attach_update('localist_event', $localist_event, $form, $form_state);

  // Alert the user that a save was successful.
  drupal_set_message(t('<em>!event_name</em> saved.', array('!event_name' => $wrapper->name->value(), PASS_THROUGH)));

  // Redirect to the view.
  $form_state['redirect'] = 'localist/event/' . $wrapper->leid->value();
}
